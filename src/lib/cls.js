import clsx from "clsx";
import { twMerge } from "tailwind-merge";

export default function cls(...args) {
  return clsx(twMerge(...args));
}
