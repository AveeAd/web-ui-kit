import axios from "axios";
const URI = import.meta.env.VITE_API_URI;

const instance = axios.create({
  baseURL: URI,
});
function getPosts() {
  return async () => await instance.get(URI + "/posts");
}

function getPostsById(id) {
  return async () => await instance.get(URI + `/posts/${id}`);
}

function postPost(data) {
  return async () => await instance.post(URI + "/posts", data);
}

const Apis = {
  getPosts,
  getPostsById,
  postPost,
};

export default Apis;
