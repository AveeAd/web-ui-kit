import cls from "../../lib/cls";
import Input from "./Input";
import Select from "./Select";

function FormWrapper({ component: InputComponent, className = "", ...rest }) {
  return (
    <div className={cls("p-2", className)}>
      <InputComponent {...rest} />
    </div>
  );
}

const FormElements = {
  Input: props => <FormWrapper {...props} component={Input} />,
  Select: props => <FormWrapper {...props} component={Select} />,
};

export default FormElements;
