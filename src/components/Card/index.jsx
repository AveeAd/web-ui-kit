import cls from "../../lib/cls";

export default function Card({ children, className = "" }) {
  return (
    <>
      <div className={cls("p-4 bg-white rounded-2xl", className)}>
        {children}
      </div>
    </>
  );
}
