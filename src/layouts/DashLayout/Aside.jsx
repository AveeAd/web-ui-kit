import cls from "../../lib/cls";
import Menu from "./Menu";
export default function Aside({ menus }) {
  return (
    <aside
      className={cls(
        "@5xl/dash:block @5xl/dash:col-span-1 @5xl/dash:static @5xl:w-auto @5xl/dash:max-w-auto @5xl/dash:p-5 @5xl/dash:h-full min-w-64 "
      )}
    >
      <div
        className={cls(
          " @5xl/dash:bg-primary-600 @5xl/dash:rounded-2xl overflow-hidden py-8 h-[80vh]"
        )}
      >
        <nav>
          {menus?.map((menu, index) => {
            return <Menu key={menu?.key || index} menu={menu} />;
          })}
          <ul></ul>
        </nav>
      </div>
    </aside>
  );
}
