import Card from "../../components/Card";
import Form from "../../components/Form";

const form = {
  columns: 4,
  groups: [
    {
      key: "group1",
      title: "Basic Details",
      rows: [
        {
          key: "row1",
          fields: [
            { id: "name1", label: "Name", type: "text" },
            {
              id: "select",
              label: "Select",
              type: "select",
              options: [
                { label: "Orange", value: "option1" },
                { label: "Red", value: "option2" },
                { label: "Blue", value: "option3" },
                { label: "Green", value: "option4" },
                { label: "Yellow", value: "option5" },
                { label: "Violet", value: "option6" },
                { label: "Pink", value: "option7" },
                { label: "Cyan", value: "option8" },
              ],
            },
          ],
        },
        {
          key: "row2",
          fields: [
            { id: "name", label: "Name", type: "text" },
            { id: "email", label: "Email", type: "email" },
            { id: "url", label: "URL", type: "url" },
            { id: "tel", label: "Tel", type: "tel" },
          ],
        },
      ],
    },
  ],
};

export default function FormPage() {
  return (
    <Card>
      <h1 className="text-2xl font-semibold text-center">Form Example</h1>
      <Form.Container form={form} />
    </Card>
  );
}
