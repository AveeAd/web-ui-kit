export default async function catchAsync(fn) {
  try {
    const res = await fn();
    return [res, null];
  } catch (error) {
    return [null, error];
  }
}
