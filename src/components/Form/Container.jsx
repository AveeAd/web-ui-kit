import { Fragment, useMemo } from "react";
import cls from "../../lib/cls";
import FormElements from "./Elements";

function FormRow({ row, basis = "" }) {
  return (
    <div
      className={cls("flex justify-between flex-1 flex-wrap", row.className)}
    >
      {row?.fields?.map(field => {
        switch (field?.type) {
          case "text":
          case "email":
          case "number":
          case "url":
          case "password":
          case "tel":
            return (
              <FormElements.Input
                key={field?.id}
                {...field}
                className={cls(basis)}
              />
            );
          case "select":
            return (
              <FormElements.Select
                key={field?.id}
                {...field}
                className={cls(basis)}
              />
            );

          default:
            return <FormElements.Input key={field?.key} {...field} />;
        }
      })}
    </div>
  );
}

export default function FormContainer({ form }) {
  const columns = useMemo(() => {
    switch (form?.columns) {
      case 1:
        return "basis-full";
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
        return `basis-1/${form?.columns}`;
      default:
        return `basis-[${Math.floor(100 / form?.columns)}%]`;
    }
  }, [form?.columns]);
  return (
    <>
      {form?.groups?.map(group => (
        <Fragment key={group?.key}>
          {group?.rows?.map(row => (
            <FormRow key={row?.key} basis={columns} row={row} />
          ))}
        </Fragment>
      ))}
    </>
  );
}
