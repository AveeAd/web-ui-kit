import plugin from "tailwindcss/plugin";
import colors from "./colors";

const customPlugin = plugin(function ({ addUtilities, addBase }) {
  addUtilities({
    ".wkit-sub-menu": {
      "max-height": 0,
      overflow: "hidden",
      transition: "all 200ms linear",
    },
    "#wkit-menu:checked ~ .wkit-sub-menu": {
      "max-height": "10rem",
    },
  });
  addBase({
    "input:focus-visible ~ label": {
      inset: "none",
      top: "-8px",
      left: "12px",
      height: "fit-content",
      width: "fit-content",
      "font-size": "12px",
      "z-index": 99,
      transition: "all 200ms linear",
      color: colors.primary[600],
      padding: "0 8px",
      "background-color": "white",
      "border-radius": "6px",
    },
    "input:not(:placeholder-shown) ~ label": {
      inset: "none",
      top: "-8px",
      left: "12px",
      height: "fit-content",
      width: "fit-content",
      "font-size": "12px",
      "z-index": 99,
      transition: "all 200ms linear",
      padding: "0 8px",
      "background-color": "white",
      "border-radius": "6px",
    },
    "#select:focus-within ~ label": {
      color: colors.primary[600],
    },
    "#options": {
      "max-height": 0,
      overflow: "hidden",
      transition: "all 200ms linear",
      position: "absolute",
      "z-index": 99999,
    },
    "#select:focus-within ~ #options": {
      "max-height": "10rem",
      overflow: "scroll",
    },
  });
});

export default customPlugin;
