/** @type {import('tailwindcss').Config} */
import ContainerQueriesPlugin from "@tailwindcss/container-queries";
import TypographyPlugin from "@tailwindcss/typography/";
import colors from "./colors";
import customPlugin from "./customPlugin.tailwind";
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  safelist: [
    "basis-full",
    "basis-1/2",
    "basis-1/3",
    "basis-1/4",
    "basis-1/5",
    "basis-1/6",
  ],
  theme: {
    extend: {
      colors: colors,
    },
  },
  darkMode: "media",
  plugins: [
    TypographyPlugin,
    // FormPlugin,
    ContainerQueriesPlugin,
    customPlugin,
  ],
};
