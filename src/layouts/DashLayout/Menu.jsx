import { useState } from "react";
import { FaChevronDown, FaChevronUp } from "react-icons/fa6";
import { NavLink } from "react-router-dom";
import cls from "../../lib/cls";

export default function Menu({ menu }) {
  const [isOpen, setIsOpen] = useState(false);

  const classes = {
    base: "p-3 border-b border-primary-700/70 text-neutral-50 hover:bg-primary-500/60",
    navlink: ({ isActive }) =>
      cls(
        classes.base,
        "p-3 border-b border-primary-700/70 text-neutral-50 block",
        isActive &&
          "text-primary-100 bg-primary-500/30 border-primary-700/70 shadow hover:bg-primary-500/60",
        "flex items-center space-x-2"
      ),
  };
  if (menu?.children?.length > 0) {
    return (
      <>
        <label
          htmlFor="wkit-menu"
          className={cls(
            classes.base,
            " w-full text-start flex justify-between items-center",
            "menu"
          )}
          onClick={() => setIsOpen(p => !p)}
        >
          <div className="flex items-center space-x-2">
            <span>{menu?.icon}</span>
            <span>{menu?.label}</span>
          </div>
          {isOpen ? <FaChevronUp /> : <FaChevronDown />}
        </label>
        <input type="checkbox" id="wkit-menu" className="hidden" hidden />
        {isOpen && (
          <div className="wkit-sub-menu">
            {menu?.children?.map((child, index) => (
              <NavLink
                key={child?.key || index}
                className={classes.navlink}
                to={child?.path}
                end
              >
                <span>{child?.icon}</span>
                <span>{child?.label}</span>
              </NavLink>
            ))}
          </div>
        )}
      </>
    );
  }
  return (
    <NavLink className={classes.navlink} to={menu?.path} end>
      <span className="p-1 border shadow-[0_0_0_2px_#ccc4] rounded-full bg-white text-primary-600">
        {menu?.icon}
      </span>
      <span className="">{menu?.label}</span>
    </NavLink>
  );
}
