import colors from "tailwindcss/colors";

export default Object.assign({
  primary: {
    50: "#edf5fa",
    100: "#c9e0ef",
    200: "#a5cce5",
    300: "#81b7da",
    400: "#5da3d0",
    500: "#3a8fc5",
    600: "#2f75a1",
    700: "#255b7e",
    800: "#1a415a",
    900: "#102736",
    950: "#050d12",
  },
  secondary: {
    50: "#f5f4f2",
    100: "#e0ddd8",
    200: "#ccc6bf",
    300: "#b7b0a5",
    400: "#a2998b",
    500: "#8e8271",
    600: "#746a5d",
    700: "#5a5348",
    800: "#403b33",
    900: "#27231f",
    950: "#0d0c0a",
  },
  danger: colors.red,
  success: colors.emerald,
  info: colors.sky,
  warning: colors.amber,
});
