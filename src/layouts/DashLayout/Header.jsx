import { FaUser } from "react-icons/fa";
import cls from "../../lib/cls";
export default function Header() {
  return (
    <header
      className={cls(
        "border-b",
        "border-b-neutral-300",
        "bg-primary-600 text-white",
        "py-2 px-4",
        "flex items-center"
      )}
    >
      <nav className={cls("flex justify-between items-center grow")}>
        <h1 className="text-2xl text-center">Hello World!</h1>
        <div className="flex items-center space-x-2">
          <div className="bg-primary-50 p-2 rounded-full border-2 border-primary-200">
            <FaUser className="text-primary-600" />
          </div>
          <div>
            <p>John Doe</p>
            <p className="text-sm">admin</p>
          </div>
        </div>
      </nav>
    </header>
  );
}
