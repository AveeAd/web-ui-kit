import { useQuery } from "@tanstack/react-query";
import { Fragment } from "react";
import Apis from "../../apis";

export default function Home() {
  const { data } = useQuery({
    queryKey: ["posts"],
    queryFn: Apis.getPosts(),
  });

  return (
    <div className="container mx-auto py-8 prose dark:prose-invert">
      <h1>Posts</h1>
      <article>
        {data?.data?.map(d => (
          <Fragment key={d.id}>
            <h2>{d.title}</h2>
            <p>{d.body}</p>
          </Fragment>
        ))}
      </article>
    </div>
  );
}
