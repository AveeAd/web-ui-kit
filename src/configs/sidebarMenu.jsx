import { FaHome, FaUser } from "react-icons/fa";

const menus = [
  { key: "posts", label: "Posts", path: "/", icon: <FaHome /> },
  { key: "forms", label: "Forms", path: "/forms", icon: <FaUser /> },
];

export default menus;
