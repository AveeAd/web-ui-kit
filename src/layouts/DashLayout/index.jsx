import { Outlet } from "react-router-dom";
import cls from "../../lib/cls";
import Aside from "./Aside";
import Header from "./Header";

export default function DashLayout({ menus }) {
  return (
    <div className="bg-neutral-100 dark:bg-neutral-900 h-screen overflow-clip flex flex-col">
      <Header />
      <div className={cls("@container/dash", "flex h-full")}>
        <Aside menus={menus} />
        <main className={cls("overflow-y-scroll grow @5xl/dash:p-5")}>
          <div>
            <Outlet />
          </div>
        </main>
      </div>
    </div>
  );
}
