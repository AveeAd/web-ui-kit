import FormContainer from "./Container";
import FormElements from "./Elements";

const Form = Object.assign("Form", {
  Container: FormContainer,
  Elements: FormElements,
});

export default Form;
