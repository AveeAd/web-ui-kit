import cls from "../../lib/cls";
export default function Input({ id, name, label, type = "text" }) {
  return (
    <>
      <div className={cls("relative input bg-white ")}>
        <input
          id={id}
          name={name}
          type={type}
          className={cls(
            "py-2 px-4 rounded-lg shadow-sm border border-neutral-300 w-full",
            "focus-visible:outline-primary-600 ",
            "text-neutral-500",
            "transition-border duration-100"
          )}
          placeholder=""
        />
        {label && (
          <label
            htmlFor={id}
            className={cls(
              "dark:text-white",
              "absolute inset-0 py-2 px-4 text-neutral-400",
              "transition-inset duration-150"
            )}
          >
            {label}
          </label>
        )}
      </div>
    </>
  );
}
