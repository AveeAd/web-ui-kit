import cls from "../../lib/cls";

export default function Button({
  children,
  className = "",
  variant = "primary",
  ...rest
}) {
  const variantClass = {
    primary: "bg-primary-600 text-white border-primary-600",
    secondary: "bg-secondary-600 text-white border-secondary-600",
    danger: "bg-danger-600 text-white border-danger-600",
    success: "bg-success-600 text-white border-success-600",
    warning: "bg-warning-600 text-white border-warning-600",
    info: "bg-info-600 text-white border-info-600",
    dark: "bg-black text-white border-black",
    light: "bg-white text-black border-white",
  };
  return (
    <>
      <button
        className={cls(
          "w-max h-max px-4 py-2 rounded-lg border shadow",
          variantClass[variant],
          className
        )}
        {...rest}
      >
        {children}
      </button>
    </>
  );
}
