import menus from "./configs/sidebarMenu";
import DashLayout from "./layouts/DashLayout";
import Form from "./pages/Form";
import Home from "./pages/Home";

/*
  #ROUTE OBJECT ATRIBUTES
  path?: string;
  index?: boolean;
  children?: React.ReactNode;
  caseSensitive?: boolean;
  id?: string;
  loader?: LoaderFunction;
  action?: ActionFunction;
  element?: React.ReactNode | null;
  hydrateFallbackElement?: React.ReactNode | null;
  errorElement?: React.ReactNode | null;
  Component?: React.ComponentType | null;
  HydrateFallback?: React.ComponentType | null;
  ErrorBoundary?: React.ComponentType | null;
  handle?: RouteObject["handle"];
  shouldRevalidate?: ShouldRevalidateFunction;
  lazy?: LazyRouteFunction<RouteObject>;
 */

const routes = [
  {
    path: "/",
    element: <DashLayout menus={menus} />,
    children: [
      {
        index: true,
        element: <Home />,
      },
      {
        path: "/forms",
        element: <Form />,
      },
    ],
  },
];

export default routes;
