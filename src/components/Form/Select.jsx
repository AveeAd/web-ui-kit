import { useRef, useState } from "react";
import { AiOutlineClose } from "react-icons/ai";
import cls from "../../lib/cls";

// Label Component
function Label({ label, id }) {
  if (!label) return null;
  return (
    <label
      htmlFor={id}
      className={cls(
        "dark:text-white",
        "absolute top-[-8px] left-[12px] h-fit w-fit text-[12px] z-[99] px-2 text-neutral-400 bg-white rounded-[6px]",
        "transition-all duration-200"
      )}
    >
      {label}
    </label>
  );
}

// Options Component
function Options({ options, handleSelectOption }) {
  return (
    <div id="options" className={cls("shadow rounded-xl", "bg-white w-full")}>
      {options?.map(option => (
        <li
          className={cls(
            " marker:content-none",
            "px-4 py-2 border-b border-neutral-200",
            "hover:bg-primary-50 hover:shadow transition-colors duration-150"
          )}
          onClick={handleSelectOption(option)}
          key={option.value}
        >
          {option.label}
        </li>
      ))}
    </div>
  );
}

function Chip({ option, deselect }) {
  return (
    <span className="text-xs py-1 px-2 bg-primary-100 rounded-2xl inline-flex items-center">
      <span>{option?.label}</span>
      <button className="ml-2" onClick={deselect(option)}>
        <AiOutlineClose />
      </button>
    </span>
  );
}

// Main Select Component
export default function Select({
  id,
  name,
  label,
  options: opts = [],
  multiple = false,
}) {
  const textRef = useRef(null);
  const [options, setOptions] = useState(opts);
  const [selectedOptions, setSelectedOptions] = useState([]);
  const [filteredOptions, setFilteredOptions] = useState([]);

  function handleChangeText(e) {
    setFilteredOptions(
      options.filter(o => o.label.match(new RegExp(e.target.innerText, "gi")))
    );
  }

  function handleSelectOption(option) {
    return () => {
      setOptions(prev => prev.filter(o => o.value !== option.value));
      setSelectedOptions([...selectedOptions, option]);
      textRef.current.innerText = "";
    };
  }

  function deselect(option) {
    return () => {
      setSelectedOptions(selectedOptions.filter(o => o.value !== option.value));
      setOptions(prev => [...prev, option]);
    };
  }
  return (
    <>
      <div className={cls("relative bg-white")}>
        <div
          id="select"
          htmlFor={id}
          className={cls(
            "py-2 px-4 rounded-lg shadow-sm border border-neutral-300 w-full min-h-10 flex flex-wrap focus-within:outline outline-primary-600",
            "text-neutral-500",
            multiple && "gap-1"
          )}
        >
          {multiple
            ? selectedOptions.length > 0 &&
              selectedOptions?.map(opt => (
                <Chip key={opt.value} option={opt} deselect={deselect} />
              ))
            : selectedOptions?.[0]?.label}
          <span
            ref={textRef}
            className={cls("grow", "px-2", "focus-visible:outline-none")}
            contentEditable
            onKeyDown={handleChangeText}
          ></span>
        </div>
        <Options
          options={filteredOptions.length > 0 ? filteredOptions : options}
          handleSelectOption={handleSelectOption}
        />
        <input
          id={id}
          list={id + "-list"}
          name={name}
          multiple
          type="text"
          hidden
        ></input>

        <Label label={label} id={id} />
      </div>
    </>
  );
}
